import React from 'react';
import Cart from '../Cart';
import styles from './FavoriteContainer.module.scss';
import Favorite from '../Favorite';

const FavoriteContainer = ({ favorite, addToFavorite }) => {

    return (
        <div className={styles.cartContainer}>
            {favorite.map(item => (
                <Favorite key={item.id} item={item} addToFavorite={addToFavorite} />
            ))}
            
        </div>
    );
}

export default FavoriteContainer;
