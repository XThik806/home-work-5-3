import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({ children, modalActivate, setCurrentProduct, product}) => {
    const handleClick = () => {
        modalActivate();
        setCurrentProduct(product);
        console.log(curentProduct);
    }
    return (
        <>
            <button className={styles.button} onClick={handleClick}>{children}</button>
        </>
    );
}

Button.propTypes = {
    children: PropTypes.string.isRequired
}

export default Button;
